import sys
from pathlib import Path
import datetime
from time import sleep
for i in range(0, 3):
    try:
        import yaml
        break
    except:
        print("installing libery")
        os.system("python3 -m pip install pyyaml")


def handlefile(config: Path):
    basepath = Path('.')
    docs = []
    for doc in "".join(config.read_text()).split("---"):
        try:
            if yaml.safe_load(doc) != None:
                docs.append(yaml.safe_load(doc))
        except yaml.YAMLError as exc:
            print(exc)
    for i, doc in enumerate(docs):
        if doc["kind"] == "Deployment":
            for y, container in enumerate(doc["spec"]["template"]["spec"]["containers"]):
                islocal, _, islatest, * \
                    dockerpath = str(container["image"]).split("/")
                dockerpath.insert(0,islatest.replace("latest", ""))
                islocal, islatest, dockerpath = True if islocal == "local" else False, True if islatest == "latest" else False, ":".join(
                    f"./{str(config).replace(config.name, '')}{str(basepath.relative_to(Path('.'))).replace('.','')}{'/'.join(dockerpath)}".replace("\\", "/").replace("././", "./").replace("//","/").split(":")[:-1])
                if islocal:
                    version = datetime.datetime.now().strftime("%d.%m.%Y.%H.%M.%S")
                    print(
                        f"docker build {dockerpath} -t {(container['image']+':').split(':')[0]+':'+version}")
                    os.system(
                        f"docker build {dockerpath} -t {(container['image']+':').split(':')[0]+':'+version}")
                    if islatest:
                        for _ in range(1, 4):
                            docs[i]['spec']['template']['spec']['containers'][y]['image'] = f"{docs[i]['spec']['template']['spec']['containers'][y]['image']}:".split(":")[
                                0]+":"+version
    yamls = []
    for doc in docs:
        yamls.append(yaml.dump(doc, default_flow_style=False))
    newfile = "---\n".join(yamls)
    config.open("w").write(newfile)
    print(".")
    os.system(f"kubectl apply -f {config.relative_to(Path('.'))}")
    print(".")


args = sys.argv
if "-?" in args:
    print(f"default it takes the first file that ends with k8s.yaml\n-d dryrun print the commands instead of executing item\n-a builds all the files in the root directory({str(Path('.').absolute())})\n-r builds all the files in the root directory({str(Path('.').absolute())}) and all the subdirectorys")
    exit()
if "-d" in args:
    class os:
        def system(command):
            print(command)
else:
    import os
if "k8s.py" in args[0]:
    args.pop(0)
basepath = Path('.')
files_in_basepath = basepath.iterdir()
files = []
for item in files_in_basepath:
    if item.is_file() and (item.name.endswith(".yaml") or item.name.endswith(".yml")):
        files.append(item)
if files.__len__() > 1 and "-a" or "-r" not in args:
    for file in files:
        if str(file.name).startswith("k8s"):
            files = [file]
            break
if "-r" in args:
    files = list(basepath.glob('**/*.yaml'))
nwln,bs = "\n","\\"
print(f"found files:\n{nwln.join([str(file).replace(bs,'/') for file in files])}\n")
for config in files:
    handlefile(config)
if "-l" in args:
    try:
        while True:
            os.system("kubectl get all")
            sleep(1)
    except KeyboardInterrupt:
        pass
